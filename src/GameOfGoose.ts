import Space from "./spaces/Space";
import SpaceFactory from "./SpaceFactory";
import BeyondSpace from "./spaces/BeyondSpace";

class GameOfGoose {
  private _board: Space[] = [];
  private readonly _totalSpaces: number;

  constructor() {
    this._totalSpaces = 63;
    this.generateBoardWithSpaces();
  }

  get board(): Space[] {
    return this._board;
  }

  get totalSpaces(): number {
    return this._totalSpaces;
  }

  private generateBoardWithSpaces() {
    for (let i = 1; i <= this._totalSpaces; i++) {
      const space = SpaceFactory.createSpace(i);
      this._board.push(space);
    }
  }

  roll(numberOfSpace: number) {
    if (this.isOutOfBoard(numberOfSpace))
      return new BeyondSpace(numberOfSpace).rule()

    const space = this.findSpaceByNumber(numberOfSpace);
    return space?.rule();
  }

  private isOutOfBoard(numberOfSpace: number) {
    if (numberOfSpace < 1)
      throw new Error('Invalid number of space')
    return numberOfSpace > 63;
  }

  private findSpaceByNumber(numberOfSpace: number) {
    return this._board.find((spaceItem) => spaceItem.space === numberOfSpace);
  }
}

export default GameOfGoose;