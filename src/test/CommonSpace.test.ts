import CommonSpace from "../spaces/CommonSpace";

describe('CommonSpace', () => {
  it('should compile', () => {
    const commonSpace = new CommonSpace(1);
    expect(commonSpace).toBeDefined();
  })
})