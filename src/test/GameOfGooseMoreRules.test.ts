import GameOfGoose from "../GameOfGoose";

describe('game of goose', () => {
  let game: GameOfGoose;
  beforeEach(() => {
    game = new GameOfGoose();
  })

  it('should create a board with 63 spaces', () => {
    expect(game?.totalSpaces).toBe(game?.board.length);
  });

  it('should returns "The Hotel: Stay for (miss) one turn"  when the space is 19', () => {
    expect(game?.roll(19)).toBe('The Hotel: Stay for (miss) one turn');
  })

  it('should returns "The Well: Wait until someone comes to pull you out - they then take your place" when the space is 31', () => {
    expect(game?.roll(31)).toBe('The Well: Wait until someone comes to pull you out - they then take your place');
  })

  it('should returns "The Maze: Go back to space 39" when the space is 42', () => {
    expect(game?.roll(42)).toBe('The Maze: Go back to space 39');
  })

  it('should returns "The Prison: Wait until someone comes to release you - they then take your place" when the space is between 50 and 55', () => {
    expect(game?.roll(50)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
    expect(game?.roll(51)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
    expect(game?.roll(52)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
    expect(game?.roll(53)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
    expect(game?.roll(54)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
    expect(game?.roll(55)).toBe('The Prison: Wait until someone comes to release you - they then take your place');
  })

})