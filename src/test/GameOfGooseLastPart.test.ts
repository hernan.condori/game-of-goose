import GameOfGoose from "../GameOfGoose";

describe('game of goose', () => {
  let game: GameOfGoose;
  beforeEach(() => {
    game = new GameOfGoose();
  })

  it('should create a board with 63 spaces', () => {
    expect(game?.totalSpaces).toBe(game?.board.length);
  });

  it('should returns "Death: Return your piece to the beginning - start the game again" when the space is 58', () => {
    expect(game?.roll(58))
      .toBe(`Death: Return your piece to the beginning - start the game again`);
  });

  it('should returns "Finish: you ended the game" when the space is 63', () => {
    expect(game?.roll(63))
      .toBe(`Finish: you ended the game`);
  });

  it('should returns "Move to space 53 and stay in prison for two turns" when the space is 64', () => {
    expect(game?.roll(64))
      .toBe(`Move to space 53 and stay in prison for two turns`);
  });
})