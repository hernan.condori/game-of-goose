import Space from "../spaces/Space";
import CommonSpace from "../spaces/CommonSpace";
import MultipleOfSixSpace from "../spaces/MultipleOfSixSpace";

describe('Space', () => {
  it('should returns "Stay in space 1" when the number is not multiple of 6', () => {
    const space = new CommonSpace(1);
    expect(space.rule()).toBe('Stay in space 1');
  })

  it('should returns "Move two spaces forward" when the number is multiple of 6', () => {
    const space = new MultipleOfSixSpace(12);
    expect(space.rule())
      .toBe(`Move two spaces forward`);
  });
})
