import GameOfGoose from "../GameOfGoose";

describe('game of goose, first rules of the game', () => {
  let game: GameOfGoose;
  beforeEach(() => {
    game = new GameOfGoose();
  })

  it('should returns "Stay in space 1" when the number is not multiple of 6', () => {
    const numberNotMultipleOfSix = 1;
    expect(game?.roll(numberNotMultipleOfSix))
      .toBe(`Stay in space ${numberNotMultipleOfSix}`);
  });

  it('should returns "Move two spaces forward" when the number is multiple of 6', () => {
    const numberMultipleOfSix = 12;
    expect(game?.roll(numberMultipleOfSix))
      .toBe(`Move two spaces forward`);
  });

  it('should returns "The Bridge: Go to space 12" when the number is 6', () => {
    expect(game?.roll(6))
      .toBe(`The Bridge: Go to space 12`);
  });
});
