import CommonSpace from "./spaces/CommonSpace";
import PrisonSpace from "./spaces/PrinsonSpace";
import MazeSpace from "./spaces/MazeSpace";
import WellSpace from "./spaces/WellSpace";
import BridgeSpace from "./spaces/BridgeSpace";
import MultipleOfSixSpace from "./spaces/MultipleOfSixSpace";
import HotelSpace from "./spaces/HotelSpace";
import DeathSpace from "./spaces/DeathSpace";
import FinishSpace from "./spaces/FinishSpace";
import BeyondSpace from "./spaces/BeyondSpace";

class SpaceFactory {
  createSpace(numberOfSpace: number) {
    
    if (this.isBetweenFiftyAndFiftyFive(numberOfSpace))
      return new PrisonSpace(numberOfSpace);

    if (this.isNumberFortyTwo(numberOfSpace))
      return new MazeSpace(numberOfSpace);

    if (this.isNumberThirtyOne(numberOfSpace))
      return new WellSpace(numberOfSpace);

    if (this.isNumberSix(numberOfSpace))
      return new BridgeSpace(numberOfSpace);

    if (this.isMultipleOfSix(numberOfSpace))
      return new MultipleOfSixSpace(numberOfSpace);

    if (this.isNumberNineteen(numberOfSpace))
      return new HotelSpace(numberOfSpace)

    if (this.isNumberFiftyEight(numberOfSpace))
      return new DeathSpace(numberOfSpace)

    if (this.isNumberSixtyThree(numberOfSpace))
      return new FinishSpace(numberOfSpace)

    if (this.isOutOfBoard(numberOfSpace))
      return new BeyondSpace(numberOfSpace)

    return new CommonSpace(numberOfSpace);
  }

  private isOutOfBoard(numberOfSpace: number) {
    return numberOfSpace > 63;
  }

  private isNumberSixtyThree(numberOfSpace: number) {
    return numberOfSpace === 63;
  }

  private isNumberFiftyEight(numberOfSpace: number) {
    return numberOfSpace === 58;
  }

  private isBetweenFiftyAndFiftyFive(space: number) {
    return space >= 50 && space <= 55;
  }

  private isNumberFortyTwo(space: number) {
    return space === 42;
  }

  private isNumberThirtyOne(space: number) {
    return space === 31;
  }

  private isNumberSix(space: number) {
    return space === 6;
  }

  private isMultipleOfSix(space: number) {
    return space % 6 === 0;
  }

  private isNumberNineteen(space: number) {
    return space === 19;
  }
}

export default new SpaceFactory()