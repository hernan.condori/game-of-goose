import Space from "./Space";

class CommonSpace extends Space {
  rule(): string {
    return `Stay in space ${this._space}`;
  }
}

export default  CommonSpace