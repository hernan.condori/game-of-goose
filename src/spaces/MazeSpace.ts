import Space from "./Space";

class MazeSpace extends Space {
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `The Maze: Go back to space 39`;
  }
}

export default  MazeSpace