import Space from "./Space";

class DeathSpace extends Space {
  rule(): string {
    return "Death: Return your piece to the beginning - start the game again";
  }
}

export default DeathSpace