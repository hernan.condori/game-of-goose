import Space from "./Space";

class BridgeSpace extends Space {
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `The Bridge: Go to space 12`;
  }
}

export default  BridgeSpace