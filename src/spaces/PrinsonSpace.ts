import Space from "./Space";

class PrisonSpace extends Space {
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `The Prison: Wait until someone comes to release you - they then take your place`;
  }
}

export default  PrisonSpace