import Space from "./Space";

class FinishSpace extends Space {
  rule() {
    return 'Finish: you ended the game'
  }
}

export default FinishSpace