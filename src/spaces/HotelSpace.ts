import Space from "./Space";

class HotelSpace extends Space{
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `The Hotel: Stay for (miss) one turn`;
  }
}

export default HotelSpace