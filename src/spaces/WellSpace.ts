import Space from "./Space";

class WellSpace extends Space {
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `The Well: Wait until someone comes to pull you out - they then take your place`;
  }
}

export default  WellSpace