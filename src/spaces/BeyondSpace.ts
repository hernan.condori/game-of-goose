import Space from "./Space";

class BeyondSpace extends Space {
  rule(): string {
    return "Move to space 53 and stay in prison for two turns";
  }
}

export default BeyondSpace