import Space from "./Space";

class MultipleOfSixSpace extends Space{
  constructor(numberOfSpace: number) {
    super(numberOfSpace);
  }

  rule(): string {
    return `Move two spaces forward`;
  }
}

export default MultipleOfSixSpace