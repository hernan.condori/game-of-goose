abstract class Space {
  protected readonly _space: number;

  constructor(numberOfSpace: number) {
    this._space = numberOfSpace
  }

  abstract rule(): string;

  get space(): number {
    return this._space;
  }
}

export default Space;